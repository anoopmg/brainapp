welcome.app.api = {}

welcome.app.api.object = {}
welcome.app.api.object.key = '8542850562';
welcome.app.api.object.qqid = '';
welcome.app.api.object.qqoid = '';
welcome.app.api.url = 'http://brainapp-api.wellcomeapps.com/?';
//welcome.app.api.url = 'http://api.welcometrust.local/?';
welcome.app.api.ajax = function(selected,valueResult)
	{
		var condata = '';
		$.ajax({
			  url: welcome.app.api.url,
			  dataType: 'jsonp',
			  async: false,
			  crossDomain:true,
			  data: welcome.app.api.object,
			  success: function(data)
				  	{
				  		welcome.app.api.showResults(selected, valueResult, data);
					},
				error: function (xhr, desc, err) {
			    		condata = 'error';
			  		}
				});

		return condata;
	}

welcome.app.api.vote = function()
	{
		
		$('.option1, .option2').live('click', 
			function() 
				{ 
					var valueResult = $(this).find('.voteButton').attr('value');
					//build ajax call
					welcome.app.api.object.qqid = $('#qqid').val();
					welcome.app.api.object.qqoid = valueResult;
					//Create url
					welcome.app.api.ajax(this, valueResult);
				});
	}

welcome.app.api.showResults= function(currentSelected, valueResult, data)
	{
		
		if(typeof data != 'undefined' || data == 'resultPast')
			{
				if(data.success == true || data == 'resultPast')
				{
					
					var voteOptions = $('#wrapper').find('.voteButton');
					
					//$('#vote #wrapper .feedbackmessage').slideDown();
					$('#vote #wrapper .shareButtons').slideDown();

					$.each(voteOptions, function(i, self)
					{
						var currentVal = $(this).val();
						var parentClass = $(this).parent().attr('class');

						if(valueResult == currentVal) 
							{
								$('.'+parentClass).addClass('votedSelected');
								$('.result'+parentClass.substr(parentClass.length - 1)).addClass('selectedResult');
								welcome.app.social.buildmessageResultpage(parentClass);
							}
					});
					$('#vote #wrapper .shareButtons').slideDown();
					if(data != 'resultPast') 
					{
						
						$.cookie('vote', valueResult, { expires: 1, path: '/' });
						$.each(data, function(i, self)
						{ 
							if($('.option1 .voteButton').val() == i) $.cookie('voteOption1', self, { expires: 1, path: '/' });
							if($('.option2 .voteButton').val() == i) $.cookie('voteOption2', self, { expires: 1, path: '/' });
						});
						
						//$.cookie('voteOption1', data[1], { expires: 1, path: '/' });
						//$.cookie('voteOption2', data[2], { expires: 1, path: '/' });
					}

				if(typeof cookieSaved == 'object')
				{	
					$.each(data, function(i, self)
						{ 
							if($('.option1 .voteButton').val() == i) $('.result1').html(self+ '%<br/> <strong>Voted</strong>');
							if($('.option2 .voteButton').val() == i) $('.result2').html(self+ '%<br/> <strong>Voted</strong>');
						});


					//$('.result1').html(data[1] + '%<br/> <strong>Voted</strong>');
					//$('.result2').html(data[2] + '%<br/> <strong>Voted</strong>');
				}
				else
				{
					var vote1 = $.cookie('voteOption1');
					var vote2 = $.cookie('voteOption2');
					$('.result1').html(vote1 + '%<br/> <strong>Voted</strong>');
					$('.result2').html(vote2 + '%<br/> <strong>Voted</strong>');	
				}


					$('.option1').addClass('optionVoted1')
					$('.optionVoted1').removeClass('option1');

					$('.option2').addClass('optionVoted2')
					$('.optionVoted2').removeClass('option2');

					
					$('.float_line_2').slideDown();
					$('.result1, .result2').slideDown();
					
					$('.optionslist a').removeClass('activeArg');
					$('.optionslist a').last().removeClass('btndisabled').addClass('activeArg');	

					// set cookie to remember vote 
					

				}
				else if(data.errors.length > 1)
				{
					
					//if(valueResult == 'y') $('.result1').addClass('selectedResult');
					//if(valueResult == 'n') $('.result2').addClass('selectedResult');

					$('.option1').addClass('optionVoted1')
					$('.optionVoted1').removeClass('option1');

					$('.option2').addClass('optionVoted2')
					$('.optionVoted2').removeClass('option2');

					$(currentSelected).addClass('votedSelected');
				}
			}
	}
welcome.app.api.showVotedResults = function(valTopast)
	{
		var hash = location.hash;
		if(hash == '#results' || valTopast == '#results')
		{
			if($.cookie('vote') !== 'null')
				{			
					var voted = $.cookie('vote');
					var valueResultpast = $('#wrapper').find('.voteButton');
						$.each(valueResultpast, 
							function(i, self)
								{ 
									if(voted == $(this).val()) 
									{
										var className = '';
										if(voted == '1') className = '.options1';
										if(voted == '2') className = '.options12';
										welcome.app.api.showResults(className, $(this).val(), 'resultPast');
									}
								});
				}
				else
				{
					welcome.app.api.vote();	
				}
		}
		else
		{
			welcome.app.api.vote();
		}

	}
welcome.app.api.activateShowVotedResults = function(valForresult)
	{
		welcome.app.api.showVotedResults(valForresult);
	}
welcome.app.api.clearCookie = function(valForresult)
	{
		$.removeCookie('vote', { path: '/' });
		$.removeCookie('voteOption1', { path: '/' });
		$.removeCookie('voteOption2', { path: '/' });
        $.removeCookie('forvisited', { path: '/' });

	}

function getLocalPath(pageName)
{
    var location = document.location.href;
    location = location.split('teachers.html').join(pageName);
    location = location.split(' ').join('%20');
    return location;
}

function newWindow()
{
    var newPath =getLocalPath("documents/How-to-guide.pdf");
    window.plugins.childBrowser.showWebPage(newPath, { showLocationBar: true })
}

$(document).ready(function() { welcome.app.api.showVotedResults(''); });