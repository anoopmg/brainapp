var welcome = {};
welcome.app = {};
welcome.app.ipad = false;
welcome.app.test = {};

$(window).load(
	function() 
		{ 
			$('.loading').delay(800).fadeOut('slow'); 
		}
	);

welcome.app.test.init = function() 
	{
		welcome.app.test.showTemplates();
		//welcome.app.test.touchEvents();
		$('.loading').show();
		welcome.app.showHide('.selects li', 'a');
		welcome.app.showHide('.imagesLists', 'li', 'hide');
		welcome.app.showHide('.multidata', '.content', 'hide');
		var imagelists = new Array();
			imagelists[0] = '.imagesLists';
			imagelists[1] = '.multidata';

		welcome.app.showHideReveal('.selects li a', imagelists);
		welcome.app.showHideRevealDefault();

		welcome.app.showHide('.hotspot_screen .left .hotspotsList', 'a');
		welcome.app.showHide('.hotspot_screen .right', '.content', 'hide');
		var imagelistsTwo = new Array();
			imagelistsTwo[0] = '.hotspotdata';
		welcome.app.showHideReveal('.hotspot_screen .left .hotspotsList a', imagelistsTwo);
		welcome.app.navigationButtons();
        welcome.app.test.showForVisited();
        welcome.app.carouselState();
	}


welcome.app.showHide = function(arg1, arg2, arg3)
	{
		if(typeof arg2 == 'undefined') arg2 = 'li';
		if(typeof arg3 == 'undefined') arg3 = '';
		var lists = $(arg1).find(arg2);
		$.each(lists, function(i, self)
			{ 
				$(this).attr('id', i);
				if(arg3 == 'hide') 
					{ 
						$(this).hide(); 
					}  
			});

		if(arg3 == 'hide') { $(arg1).find('.default').slideDown(); }

	}


welcome.app.showHideReveal = function(clickToReveal, reveal)
	{
		
		$(clickToReveal).live('click', function(){ 
			var clickedId = this.id;
			
			var activeElements = $(this).parent().parent().find('a');

			$.each(activeElements, function() { $(this).removeClass('active'); });
			$(this).addClass('active');
			$.each(reveal, function(i, self)
				{ 
					/* HIde dafult state */
					$(self).parent().find('.default').hide('fast');
					$(self).children().find('.default').hide('fast');

					/* hide all to reveal correct state clicked by user */
					$(self).children().hide('fast');

					/* Reveal relevant content */
					$(self).find('#'+clickedId).slideDown('slow')
				}); 

		});
	}
welcome.app.showHideRevealDefault = function()
	{
		
		$('.backtodefault').live('click', function(){ 
			var clickedId = this.id;
			
			$('.left').find('li').hide();
			$('.right').find('.content').hide();
			$('.right').find('.default').slideDown();
			$('.left').find('.default').slideDown('slow');
		});
	}


welcome.app.navigationButtons = function()
	{
		var cookieSaved = $.cookie('vote');
		var cookieSavedFor = $.cookie('forvisited');
		if(typeof cookieSaved == 'object')
		{	
			$('.resultsButton').addClass('btndisabled');
			$('.resultsButton').attr('href','#');
			//$('.votebutton').addClass('btndisabled');
			//$('.votebutton').attr('href','#');

		}
		else
		{
			//:onClick => "welcome.app.api.activateShowVotedResults('#results');
			$('.votebutton').removeClass('btndisabled');
			$('.resultsButton').attr("onClick", "welcome.app.api.activateShowVotedResults('#results');");
			$('.resultsButton').removeClass('btndisabled');		
		}
		if(typeof $('.votebutton').attr('class') != 'undefined') 
			{
				if($('.votebutton').attr('class').search('btndisabled') != '-1' && cookieSavedFor != '345689000')
					{
						$('.votebutton').attr('href','#');
					}
			}
        
	} 
welcome.app.test.showForVisited = function()
    {
        var urlCheck = window.location.href;
        if(urlCheck.toString().indexOf('for.html') !== -1)
        {
            var cookieSavedFor = $.cookie('forvisited');
            if(cookieSavedFor == '345689000')
            {	
                $('.votebutton').removeClass('btndisabled');
               
            }
            else
            {
                $.cookie('forvisited', '345689000', { expires: 1, path: '/' });
            }
        }
    }
welcome.app.test.showTemplates = function()
	{ 
		$(".viewTemplate").click(function(){
			if($(".testTemplates ul").css('display') == 'none')
			{
				$(".testTemplates ul").slideDown();
			}
			else
			{
				$(".testTemplates ul").slideUp();
			}
			
		});
	}

welcome.app.carouselState = function()
	{
		//numberModule

		var visitedCarousels = $.cookie('visitedModules');

		if(visitedCarousels != null)
		{
			if($('#numberModule').val() != null)
				{
					var vivitedCSplit = visitedCarousels.split('|');
						vivitedCSplit.push($('#numberModule').val());
						vivitedCSplit = welcome.app.eliminateDuplicates(vivitedCSplit).join('|');
						$.cookie('visitedModules', vivitedCSplit, { expires: 1, path: '/' });
				}	
		}
		else
		{
			if($('#numberModule').val() != null)
				{
					$.cookie('visitedModules', $('#numberModule').val(), { expires: 1, path: '/' });	
				}				
			
		}
		
		if($('#vote .carousel').length > 0)
		{
			welcome.app.api.clearCookie();
			if(visitedCarousels != null)
			{
				var vivitedCSplit = visitedCarousels.split('|');
				var countSlides = $('#vote #wrapper .carousel .slides').find('div');
				if(countSlides.length == vivitedCSplit.length) {
					$.removeCookie('visitedModules', { path: '/' });
					welcome.app.setupCarousel();

				}
				else
				{
					visitedCarousels = $.cookie('visitedModules');
					$.each(vivitedCSplit, 
						function(i, self) 
							{ 
								var imgSRC = $('#vote #wrapper .carousel .slides #'+self+' a img');
									newImgSrc = imgSRC.attr('src').split('.png').join('_visited.png');
									imgSRC.attr('src', newImgSrc);
							});
					var num = 0;
					var slideTogoTo = new Array();
					for(i = 0; i < countSlides.length; i++)
					{
						
						if(vivitedCSplit.join('|').search(i) === -1) slideTogoTo = i;
					}
					welcome.app.setupCarousel(slideTogoTo);
				}
				
			}
			else
			{
				welcome.app.api.clearCookie();
				welcome.app.setupCarousel();
			}

		}
		

	}

welcome.app.setupCarousel = function(slideto){
	if(typeof slideto == 'undefined') slideto = null;
	var buttonnavshow = true;
	$('.carousel').carousel({hAlign: 'center',
			vAlign: 'center',
			hMargin: 0.4,
			vMargin: 0.2,
			frontWidth: 342,
			frontHeight: 377,
			carouselWidth: 700,
			carouselHeight: 400,
			left: 0,
			right: 0,
			top: 0,
			bottom: 0,
			backZoom: 0.8,
			slidesPerScroll: 7,
			speed: 500,
			buttonNav: false,
			directionNav: buttonnavshow,
			autoplay: false,
			autoplayInterval: 5000,
			pauseOnHover: true,
			mouse: true,
			shadow: false,
			reflection: false,
			reflectionHeight: 0.2,
			reflectionOpacity: 0.5,
			reflectionColor: '255,255,255',
			description: true,
			descriptionContainer: '.description',
			backOpacity: 1,
			slideToGoTo: slideto});
}
welcome.app.eliminateDuplicates = function(arr) {
  var i,
      len=arr.length,
      out=[],
      obj={};

  for (i=0;i<len;i++) {
    obj[arr[i]]=0;
  }
  for (i in obj) {
    out.push(i);
  }
  return out;
}
welcome.app.childbrowser = function(id)
{
    window.plugins.childBrowser.showWebPage($("#"+id).attr('href'), { showLocationBar: true });
    return false;
}